package edu.ndsu.cs.hayden.christensonAssignment3.cayenne.persistent;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import edu.ndsu.cs.hayden.christensonAssignment3.cayenne.persistent.auto._Advisor;

public class Advisor extends _Advisor {

    private static final long serialVersionUID = 1L; 

    public Integer getPK()
    {
    	if(getObjectId() != null && !getObjectId().isTemporary())
    	{
    		return (Integer) getObjectId().getIdSnapshot().get(PK_PK_COLUMN);
    	}
    	return null; 
    }
    
    private Advisor getAdvisorInstance() {
    	Random rand = new Random();
    	String[] firstNames = {"Anne", "Bob", "Carl", "Deb", "Ed", "Fran", "Greg", "Heidi", "Ian"}; 
    	String[] lastNames  = {"Adams", "Smith", "Jones", "Baker", "Evans", "Davies", "Fritz", "Carlson"}; 
    	String[] majors     = {"Math", "CS", "Biology", "Engineering", "Chemistry", "Physics", "Geology"};
    		
    	Advisor advisor = new Advisor();
    	advisor.setName(firstNames[rand.nextInt(firstNames.length)] + " " + lastNames[rand.nextInt(lastNames.length)]);
    	Date date = new Date();
    	advisor.setDateOfBirth(date);
    	advisor.setMajor(majors[rand.nextInt(majors.length)] + " " + (rand.nextInt(300) + 100));
    		
    	return advisor; 
    }
    
    ArrayList<Advisor> advisors = new ArrayList<>();{
    
		for(int i = 0; i < 20; i++) {
			this.advisors.add(getAdvisorInstance());
		}

    }

}
