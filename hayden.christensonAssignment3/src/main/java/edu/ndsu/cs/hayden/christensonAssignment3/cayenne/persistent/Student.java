package edu.ndsu.cs.hayden.christensonAssignment3.cayenne.persistent;

import java.util.ArrayList;
import java.util.Random;
import java.util.Date;

import edu.ndsu.cs.hayden.christensonAssignment3.cayenne.persistent.auto._Student;

public class Student extends _Student {

    private static final long serialVersionUID = 1L; 

    
    public Integer getPK()
    {
    	if(getObjectId() != null && !getObjectId().isTemporary())
    	{
    		return (Integer) getObjectId().getIdSnapshot().get(PK_PK_COLUMN);
    	}
    	return null; 
    }
    
    private Student getStudentInstance() {
    	Random rand = new Random();
    	String[] firstNames = {"Anne", "Bob", "Carl", "Deb", "Ed", "Fran", "Greg", "Heidi", "Ian"}; 
    	String[] lastNames  = {"Adams", "Smith", "Jones", "Baker", "Evans", "Davies", "Fritz", "Carlson"}; 
    	String[] majors     = {"Math", "CS", "Biology", "Engineering", "Chemistry", "Physics", "Geology"};
    		
    	Student student = new Student();
    	student.setName(firstNames[rand.nextInt(firstNames.length)] + " " + lastNames[rand.nextInt(lastNames.length)]);
    	Date date = new Date();
    	student.setDateOfBirth(date);
    	double random = new Random().nextDouble();
    	double gpa = random * 4;
    	student.setGpa(gpa);
    	student.setMajor(majors[rand.nextInt(majors.length)] + " " + (rand.nextInt(300) + 100));
    	int randomid = new Random().nextInt(999999);
    	student.setStudentID(randomid);

    	return student; 
    }
    
    ArrayList<Student> students = new ArrayList<>();{
    
		for(int i = 0; i < 20; i++) {
			this.students.add(getStudentInstance());
		}

    }
    
}
