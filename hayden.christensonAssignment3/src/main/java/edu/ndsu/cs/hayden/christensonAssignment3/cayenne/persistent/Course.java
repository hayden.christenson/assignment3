package edu.ndsu.cs.hayden.christensonAssignment3.cayenne.persistent;

import java.util.ArrayList;
import java.util.Random;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.access.DataContext;
import org.apache.cayenne.configuration.CayenneRuntime;

import edu.ndsu.cs.hayden.christensonAssignment3.cayenne.persistent.auto._Course;
import edu.ndsu.cs.hayden.christensonAssignment3.services.CayenneService;

public class Course extends _Course {

    private static final long serialVersionUID = 1L; 

    public Integer getPK()
    {
    	if(getObjectId() != null && !getObjectId().isTemporary())
    	{
    		return (Integer) getObjectId().getIdSnapshot().get(PK_PK_COLUMN);
    	}
    	return null; 
    }
    
    private Course getCourseInstance() {
    	Random rand = new Random();
    	String[] firstNames = {"Anne", "Bob", "Carl", "Deb", "Ed", "Fran", "Greg", "Heidi", "Ian"}; 
    	String[] lastNames  = {"Adams", "Smith", "Jones", "Baker", "Evans", "Davies", "Fritz", "Carlson"}; 
    	String[] majors     = {"Math", "CS", "Biology", "Engineering", "Chemistry", "Physics", "Geology"};
    		
    	Course course = new Course();
    	course.setName(majors[rand.nextInt(majors.length)] + " " + (rand.nextInt(300) + 100));
    	course.setInstructor(firstNames[rand.nextInt(firstNames.length)] 
    		+ " " + lastNames[rand.nextInt(lastNames.length)]);
    	course.setCapacity((rand.nextInt(40) + 1) * 5);
    		
    	return course; 
    }
    
    ArrayList<Course> courses = new ArrayList<>();{
    
		for(int i = 0; i < 20; i++) {
			this.courses.add(getCourseInstance());
		}

    }
}
