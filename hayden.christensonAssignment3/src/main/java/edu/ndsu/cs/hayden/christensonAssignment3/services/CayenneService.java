package edu.ndsu.cs.hayden.christensonAssignment3.services;

import org.apache.cayenne.ObjectContext;

public interface CayenneService 
{
	ObjectContext newContext();
}
