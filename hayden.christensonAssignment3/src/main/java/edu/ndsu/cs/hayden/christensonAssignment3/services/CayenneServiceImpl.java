package edu.ndsu.cs.hayden.christensonAssignment3.services;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;

import edu.ndsu.cs.hayden.christensonAssignment3.cayenne.persistent.Advisor;
import edu.ndsu.cs.hayden.christensonAssignment3.cayenne.persistent.Course;
import edu.ndsu.cs.hayden.christensonAssignment3.cayenne.persistent.Student;

public class CayenneServiceImpl implements CayenneService {

	private ServerRuntime cayenneRuntime; 
	
	public CayenneServiceImpl() {
		cayenneRuntime = ServerRuntime.builder().addConfig("cayenne-hayden.christensonAssignment3.xml").build();
	}

	@Override
	public ObjectContext newContext() {
		return cayenneRuntime.newContext();
	}	
}
