package edu.ndsu.cs.hayden.christensonAssignment3.pages.students;

import org.apache.cayenne.Cayenne;
import org.apache.cayenne.ObjectContext;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

import edu.ndsu.cs.hayden.christensonAssignment3.services.CayenneService;
import edu.ndsu.cs.hayden.christensonAssignment3.cayenne.persistent.Student;

public class ViewStudents {
	@Inject
	CayenneService cayenneService;
	
	@Property
	@Persist
	int PK;
	
	@Property
	@Persist
	Student student;
	
	void onActivate(int PK) {
		this.PK = PK;
	}
	
	int onPassivate() {
		return PK;
	}
	
	void setupRender() {
		ObjectContext context = cayenneService.newContext();
		student = Cayenne.objectForPK(context, Student.class, PK);
	}
}
