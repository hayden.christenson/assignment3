package edu.ndsu.cs.hayden.christensonAssignment3.pages;

import java.util.List;

import org.apache.cayenne.Cayenne;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.query.ObjectSelect;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

import edu.ndsu.cs.hayden.christensonAssignment3.services.CayenneService;
import edu.ndsu.cs.hayden.christensonAssignment3.cayenne.persistent.Student;

public class Index {

	@Inject
	CayenneService cayenneService; 
	
	@Property
	List<Student> students; 
	
	@Property
	Student student; 
	
	void setupRender() {
		ObjectContext context = cayenneService.newContext();
		students = ObjectSelect.query(Student.class).select(context);
	}
	
	void onDelete(int PK) {
		ObjectContext context = cayenneService.newContext();
		context.deleteObject(Cayenne.objectForPK(context, Student.class, PK));
		context.commitChanges();
	}
}
