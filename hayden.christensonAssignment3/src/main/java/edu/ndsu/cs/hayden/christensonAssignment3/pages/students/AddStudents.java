package edu.ndsu.cs.hayden.christensonAssignment3.pages.students;

import org.apache.cayenne.ObjectContext;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

import edu.ndsu.cs.hayden.christensonAssignment3.services.CayenneService;
import edu.ndsu.cs.hayden.christensonAssignment3.cayenne.persistent.Student;
import edu.ndsu.cs.hayden.christensonAssignment3.pages.Index;

public class AddStudents {
	
	@Inject
	CayenneService cayenneService;
	
	@Property
	@Persist
	Student student; 
	
	void setupRender() {
		ObjectContext context = cayenneService.newContext();
		student = context.newObject(Student.class);
	}
	
	Object onSubmitFromAddForm() {
		student.getObjectContext().commitChanges();
		return Index.class;
	}
}

